total_fuel = 0
with open('input.txt', 'r') as f:
    for num in map(int, f):
        temp = num
        while temp > 0:
            temp = int(temp / 3) -2
            if temp > 0:
                total_fuel +=temp

    print(total_fuel)
